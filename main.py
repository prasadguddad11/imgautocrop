import cv2
import numpy as np
import mapper
import fastapi
import base64
import json
import uvicorn
from fastapi import FastAPI

app = FastAPI()
if __name__ == '__main__':
    #uvicorn.run("app.app:app", port=8080, reload=False)
    uvicorn.run("main:app", port=8080, reload=False)
 

@app.get("/test1")
async def root():
    return {"message": "Hello World"}


@app.get("/cropwithrotation")
async def test(baseImg:str):
    temp = baseImg
    print("recevied=" + temp)

    try:
        with open('result2.txt', 'w') as gfg:
            gfg.write(temp)
    except Exception as e:
        print("There is a Problem", str(e))

    file = open('result2.txt', 'rb')
    encoded_data = file.read()
    file.close()

    # decode base64 string data
    decoded_data = base64.b64decode((encoded_data))
    # write the decoded data back to original format in  file
    img_file = open('image1.png', 'wb')
    img_file.write(decoded_data)
    img_file.close()

    image=cv2.imread("image1.png")   #read in the image
    image=cv2.resize(image,(1300,800)) #resizing because opencv does not work well with bigger images
    orig=image.copy()

    gray=cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)  #RGB To Gray Scale
    #cv2.imshow("Title",gray)

    blurred=cv2.GaussianBlur(gray,(5,5),0)  #(5,5) is the kernel size and 0 is sigma that determines the amount of blur
    #cv2.imshow("Blur",blurred)

    edged=cv2.Canny(blurred,30,50)  #30 MinThreshold and 50 is the MaxThreshold
    #cv2.imshow("Canny",edged)


    contours,hierarchy=cv2.findContours(edged,cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)  #retrieve the contours as a list, with simple apprximation model
    contours=sorted(contours,key=cv2.contourArea,reverse=True)

    #the loop extracts the boundary contours of the page
    for c in contours:
        p=cv2.arcLength(c,True)
        approx=cv2.approxPolyDP(c,0.02*p,True)

        if len(approx)==4:
            target=approx
            break
    approx=mapper.mapp(target) #find endpoints of the sheet

    #pts=np.float32([[0,0],[800,0],[800,800],[0,800]])  #map to 800*800 target window

    #pts=np.float32([[0,0],[2480,0],[2480,3508],[0,3508]])

    pts=np.float32([[0,0],[595,0],[595,842],[0,842]])

    op=cv2.getPerspectiveTransform(approx,pts)  #get the top or bird eye view effect
    #dst=cv2.warpPerspective(orig,op,(800,800))
    #dst=cv2.warpPerspective(orig,op,(2480,3508))
    dst=cv2.warpPerspective(orig,op,(595,842))

    filename="final.png"
    #cv2.imshow("Scanned",dst)
    cv2.imwrite(filename, dst)
    # press q or Esc to close

    # converting image to to base 64 string
    with open(filename, "rb") as f:
        im_b64 = base64.b64encode(f.read())
        output = str(im_b64, 'UTF-8')
        data = {
            "basedata":{
                "message": output
            }
        }

    json_string = json.dumps(data)
    

    #print("final=" + json_string)
    return json_string

    cv2.waitKey(0)
    cv2.destroyAllWindows()